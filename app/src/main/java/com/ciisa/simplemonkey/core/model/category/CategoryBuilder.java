package com.ciisa.simplemonkey.core.model.category;

public class CategoryBuilder {
    int id;
    String name;
    String description;
    String color;
    String icon;
    boolean expense;
    boolean income;
    double totalExpenses;
    double totalIncomes;

    public CategoryBuilder() {

    }

    public CategoryBuilder(String name, String description, String color, String icon) {
        this.name = name;
        this.description = description;
        this.color = color;
        this.icon = icon;
    }

    public CategoryBuilder withId(int id) {
        this.id = id;
        return this;
    }

    public CategoryBuilder withConditionals(boolean expense, boolean income) {
        this.expense = expense;
        this.income = income;
        return this;
    }

    public CategoryBuilder withMovements(double totalExpenses, double totalIncome) {
        this.totalExpenses = totalExpenses;
        this.totalIncomes = totalIncome;
        return this;
    }

    public Category build() {
        return new Category(this);
    }

    public CategoryDTO buildDTO() {
        return new CategoryDTO(this);
    }

    public CategoryEntity buildEntity() {
        return new CategoryEntity(this);
    }

    public CategoryViewModel buildView() {
        return new CategoryViewModel(this);
    }
}
