package com.ciisa.simplemonkey.core.controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {
    public AdminSQLiteOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // FIXME: Refactor CHAR ID Lenght

    // FIXME: Refactor Category ID for debt

    // FIXME: Make category Mandatory

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE category (\n" +
                "    id INTEGER PRIMARY KEY,\n" +
                "    name VARCHAR(50) NOT NULL,\n" +
                "    description TEXT,\n" +
                "    color VARCHAR(30),\n" +
                "    icon VARCHAR(20),\n" +
                "    income BOOLEAN NOT NULL,\n" +
                "    expense BOOLEAN NOT NULL,\n" +
                "    created_at INTEGER,\n" +
                "    updated_at INTEGER\n" +
                ")");
        db.execSQL("CREATE TABLE debt (\n" +
                "    id CHAR(36) PRIMARY KEY,\n" +
                "    uid VARCHAR(32) NOT NULL,\n" +
                "    name VARCHAR(50) NOT NULL,\n" +
                "    description TEXT,\n" +
                "    date DATE NOT NULL,\n" +
                "    amount REAL NOT NULL,\n" +
                "    currency TEXT NOT NULL,\n" +
                "    payday INTEGER,\n" +
                "    fee_counts INTEGER,\n" +
                "    done BOOLEAN DEFAULT 'FALSE',\n" +
                "    sync BOOLEAN DEFAULT 'FALSE',\n" +
                "    coordinates TEXT,\n" +
                "    created_at INTEGER DEFAULT (STRFTIME('%s', 'now')),\n" +
                "    updated_at INTEGER DEFAULT (STRFTIME('%s', 'now'))\n" +
                ")");
        db.execSQL("CREATE TRIGGER updated_at_debt\n" +
                "    AFTER UPDATE ON debt\n" +
                "BEGIN\n" +
                "    UPDATE debt\n" +
                "    SET updated_at = STRFTIME('%s', 'now')\n" +
                "    WHERE id = old.id;\n" +
                "END");
        db.execSQL("CREATE TABLE borrow (\n" +
                "    id CHAR(36) PRIMARY KEY,\n" +
                "    uid VARCHAR(32) NOT NULL,\n" +
                "    name VARCHAR(50) NOT NULL,\n" +
                "    description TEXT,\n" +
                "    debtor_name TEXT,\n" +
                "    debtor_email TEXT,\n" +
                "    date DATE NOT NULL,\n" +
                "    amount REAL NOT NULL,\n" +
                "    currency TEXT NOT NULL,\n" +
                "    payday INTEGER,\n" +
                "    fee_counts INTEGER,\n" +
                "    done BOOLEAN DEFAULT 'FALSE',\n" +
                "    sync BOOLEAN DEFAULT 'FALSE',\n" +
                "    coordinates TEXT,\n" +
                "    created_at INTEGER DEFAULT (STRFTIME('%s', 'now')),\n" +
                "    updated_at INTEGER DEFAULT (STRFTIME('%s', 'now'))\n" +
                ")");
        db.execSQL("CREATE TRIGGER updated_at_borrow\n" +
                "    AFTER UPDATE ON borrow\n" +
                "BEGIN\n" +
                "    UPDATE borrow\n" +
                "    SET updated_at = STRFTIME('%s', 'now')\n" +
                "    WHERE id = old.id;\n" +
                "END");
        db.execSQL("CREATE TABLE saving (\n" +
                "    id CHAR(36) PRIMARY KEY,\n" +
                "    uid VARCHAR(32) NOT NULL,\n" +
                "    name VARCHAR(50) NOT NULL,\n" +
                "    description TEXT,\n" +
                "    date DATE NOT NULL,\n" +
                "    amount REAL NOT NULL,\n" +
                "    currency TEXT NOT NULL,\n" +
                "    fee_counts INTEGER,\n" +
                "    done BOOLEAN DEFAULT 'FALSE',\n" +
                "    sync BOOLEAN DEFAULT 'FALSE',\n" +
                "    coordinates TEXT,\n" +
                "    created_at INTEGER DEFAULT (STRFTIME('%s', 'now')),\n" +
                "    updated_at INTEGER DEFAULT (STRFTIME('%s', 'now'))\n" +
                ")");
        db.execSQL("CREATE TRIGGER updated_at_saving\n" +
                "    AFTER UPDATE ON saving\n" +
                "BEGIN\n" +
                "    UPDATE saving\n" +
                "    SET updated_at = STRFTIME('%s', 'now')\n" +
                "    WHERE id = old.id;\n" +
                "END");
        db.execSQL("CREATE TABLE budget (\n" +
                "    id CHAR(36) PRIMARY KEY,\n" +
                "    uid VARCHAR(32) NOT NULL,\n" +
                "    name VARCHAR(50) NOT NULL,\n" +
                "    description TEXT,\n" +
                "    date DATE NOT NULL,\n" +
                "    amount REAL NOT NULL,\n" +
                "    currency TEXT NOT NULL,\n" +
                "    months INTEGER,\n" +
                "    active BOOLEAN DEFAULT 'TRUE',\n" +
                "    sync BOOLEAN DEFAULT 'FALSE',\n" +
                "    coordinates TEXT,\n" +
                "    created_at INTEGER DEFAULT (STRFTIME('%s', 'now')),\n" +
                "    updated_at INTEGER DEFAULT (STRFTIME('%s', 'now'))\n" +
                ")");
        db.execSQL("CREATE TRIGGER updated_at_budget\n" +
                "    AFTER UPDATE ON budget\n" +
                "BEGIN\n" +
                "    UPDATE budget\n" +
                "    SET updated_at = STRFTIME('%s', 'now')\n" +
                "    WHERE id = old.id;\n" +
                "END");
        db.execSQL("CREATE TABLE movement (\n" +
                "    id CHAR(36) PRIMARY KEY,\n" +
                "    uid VARCHAR(32) NOT NULL,\n" +
                "    name VARCHAR(50) NOT NULL,\n" +
                "    description TEXT,\n" +
                "    date DATE NOT NULL,\n" +
                "    amount REAL NOT NULL,\n" +
                "    currency VARCHAR(3) NOT NULL,\n" +
                "    fee_number INTEGER,\n" +
                "    payday INTEGER,\n" +
                "    done BOOLEAN DEFAULT 'TRUE',\n" +
                "    income BOOLEAN DEFAULT 'FALSE',\n" +
                "    sync BOOLEAN DEFAULT 'FALSE',\n" +
                "    coordinates TEXT,\n" +
                "    created_at INTEGER DEFAULT (STRFTIME('%s', 'now')),\n" +
                "    updated_at INTEGER DEFAULT (STRFTIME('%s', 'now')),\n" +
                "    id_category INTEGER,\n" +
                "    id_budget VARCHAR(64),\n" +
                "    id_debt VARCHAR(64),\n" +
                "    id_borrow VARCHAR(64),\n" +
                "    FOREIGN KEY(id_category) REFERENCES category(id)\n" +
                "        ON UPDATE CASCADE\n" +
                "        ON DELETE RESTRICT,\n" +
                "    FOREIGN KEY(id_budget) REFERENCES budget(id)\n" +
                "        ON UPDATE CASCADE\n" +
                "        ON DELETE SET NULL,\n" +
                "    FOREIGN KEY(id_debt) REFERENCES debt(id)\n" +
                "        ON UPDATE CASCADE\n" +
                "        ON DELETE SET NULL,\n" +
                "    FOREIGN KEY(id_borrow) REFERENCES borrow(id)\n" +
                "        ON UPDATE CASCADE\n" +
                "        ON DELETE SET NULL\n" +
                ")");

        // Trigger para ingresar un nuevo movimiento a la cola
        db.execSQL("CREATE TRIGGER created_at_movement\n" +
                "    AFTER INSERT ON movement\n" +
                "    WHEN NOT new.sync\n" +
                "    BEGIN\n" +
                "        INSERT INTO sync_queue_movement(id, uid, update_date)\n" +
                "            VALUES (new.id, new.uid, STRFTIME('%s', 'now'));\n" +
                "    END");

        // Trigger para actualizar la fecha de actualización
        db.execSQL("CREATE TRIGGER updated_at_movement\n" +
                "    AFTER UPDATE ON movement\n" +
                "    WHEN NOT new.sync\n" +
                "    BEGIN\n" +
                "        UPDATE movement\n" +
                "            SET updated_at = STRFTIME('%s', 'now')\n" +
                "            WHERE id = old.id;\n" +
                "    END");

        // Cola de sincronización para Movement
        db.execSQL("CREATE TABLE sync_queue_movement (\n" +
                "    id CHAR(36) PRIMARY KEY NOT NULL,\n" +
                "    uid VARCHAR(32) NOT NULL,\n" +
                "    update_date INTEGER DEFAULT (STRFTIME('%s', 'now')),\n" +
                "    remove BOOLEAN DEFAULT 0\n" +
                ")");

        // Si es eliminado de la cola, entonces es declarado como sincronizado
        db.execSQL("CREATE TRIGGER sync_done_movement\n" +
                "    AFTER DELETE ON sync_queue_movement\n" +
                "    WHEN NOT old.remove\n" +
                "    BEGIN\n" +
                "        UPDATE movement\n" +
                "            SET sync = 1\n" +
                "            WHERE id = old.id;\n" +
                "    END");

        // Trigger para ingresar un update de un movimiento a la cola
        db.execSQL("CREATE TRIGGER sync_pending_movement\n" +
                "    AFTER UPDATE ON movement\n" +
                "    WHEN NOT new.sync\n" +
                "    BEGIN\n" +
                "        INSERT OR REPLACE INTO sync_queue_movement(id, uid, update_date)\n" +
                "            VALUES (new.id, new.uid, STRFTIME('%s', 'now'));\n" +
                "    END");

        // Ingresa un movimiento a la cola como preparado para eliminar de la nube.
        db.execSQL("CREATE TRIGGER sync_delete_movement\n" +
                "    AFTER DELETE ON movement\n" +
                "    BEGIN\n" +
                "        INSERT OR REPLACE INTO sync_queue_movement(id, uid, update_date, remove)\n" +
                "            VALUES (old.id, old.uid, STRFTIME('%s', 'now'), 1);\n" +
                "    END");

        // Inserta la categoría OTROS
        db.execSQL("INSERT INTO category (id, name, description, color, icon, income, expense)\n" +
                "VALUES (100, 'Otro', 'Categoría otros', '#2D3A3A', 'nothing', 'true', 'true');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
