package com.ciisa.simplemonkey.core.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ciisa.simplemonkey.BuildConfig;
import com.ciisa.simplemonkey.core.controller.AdminSQLiteOpenHelper;

import java.util.ArrayList;

public class MovementSyncDAO {
    private Context context;
    private String uid;
    private AdminSQLiteOpenHelper admin;
    private String dbName = BuildConfig.DB_NAME;

    public MovementSyncDAO(Context context, String uid) {
        this.context = context;
        this.uid = uid;
    }

    public ArrayList<String> findAllToSave () {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        Cursor rows = database.rawQuery("SELECT " +
                        "id" +
                        "FROM sync_queue_movement " +
                        "WHERE uid = ? AND NOT remove " +
                        "ORDER BY update_date DESC",
                new String[] {uid});

        ArrayList<String> resultList = new ArrayList<>();

        while(rows.moveToNext()) {
            resultList.add(rows.getString(0));
        }

        database.close();
        return resultList;
    }

    public ArrayList<String> findAllToRemove () {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        Cursor rows = database.rawQuery("SELECT " +
                        "id" +
                        "FROM sync_queue_movement " +
                        "WHERE uid = ? AND remove " +
                        "ORDER BY update_date DESC",
                new String[] {uid});

        ArrayList<String> resultList = new ArrayList<>();

        while(rows.moveToNext()) {
            resultList.add(rows.getString(0));
        }

        database.close();
        return resultList;
    }

    public String delete (String id) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        int qty = database.delete("sync_queue_movement", "id = ?" ,  new String[] {id});
        database.close();
        return qty > 0 ? id : null;
    }
}
