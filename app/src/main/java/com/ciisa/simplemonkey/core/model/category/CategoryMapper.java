package com.ciisa.simplemonkey.core.model.category;

import com.ciisa.simplemonkey.core.model.movement.Movement;

import java.util.ArrayList;
import java.util.List;

public class CategoryMapper {

    /* ========== Base ======== */

    public static Category toBase(ICategory category) {
        return category != null ? new Category(category) : null;
    }

    public static ArrayList<Category> toBase(List<ICategory> iCategories) {
        final ArrayList<Category> baseCategories = new ArrayList<>();
        if (iCategories != null) {
            for (ICategory category : iCategories) {
                Category convertedCategory = toBase(category);
                baseCategories.add(convertedCategory);
            }
        }
        return baseCategories;
    }

    /* ========== DTO ======== */

    public static CategoryDTO toDTO(ICategory category) {
        return category != null ? new CategoryDTO(category) : null;
    }

    public static ArrayList<CategoryDTO> toDTO(List<ICategory> iCategories) {
        final ArrayList<CategoryDTO> dtoCategories = new ArrayList<>();
        if (iCategories != null) {
            for (ICategory category : iCategories) {
                CategoryDTO convertedCategory = toDTO(category);
                dtoCategories.add(convertedCategory);
            }
        }
        return dtoCategories;
    }

    /* ========== Entity ======== */

    public static CategoryEntity toEntity(ICategory category) {
        return category != null ? new CategoryEntity(category) : null;
    }

    public static ArrayList<CategoryEntity> toEntity(List<CategoryDTO> categories) {
        final ArrayList<CategoryEntity> entityCategories = new ArrayList<>();
        if (categories != null) {
            for (CategoryDTO category : categories) {
                CategoryEntity convertedCategory = toEntity(category);
                entityCategories.add(convertedCategory);
            }
        }
        return entityCategories;
    }

    /* ========== View ======== */

    public static CategoryViewModel toViewModel(ICategory category) {
        return category != null ? new CategoryViewModel(category) : null;
    }

    public static ArrayList<CategoryViewModel> toViewModel(List<CategoryEntity> categories) {
        final ArrayList<CategoryViewModel> viewCategories = new ArrayList<>();
        if (categories != null) {
            for (CategoryEntity category : categories) {
                CategoryViewModel convertedCategory = toViewModel(category);
                viewCategories.add(convertedCategory);
            }
        }
        return viewCategories;
    }

}
