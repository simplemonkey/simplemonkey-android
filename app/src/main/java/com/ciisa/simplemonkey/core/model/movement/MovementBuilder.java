package com.ciisa.simplemonkey.core.model.movement;

import com.ciisa.simplemonkey.core.model.category.ICategory;

import java.util.Date;

public class MovementBuilder {
    String id;
    String uid;
    String name;
    String description;
    String date;
    double amount;
    String currency;
    boolean income;
    boolean done;
    boolean sync;
    int createdAt;
    int updatedAt;
    String coordinates;
    int feeNumber;
    int payday;
    ICategory category;

    public MovementBuilder(String uid, String name, String description, String date, double amount, String currency, boolean income, boolean done) {
        this.uid = uid;
        this.name = name;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.currency = currency;
        this.income = income;
        this.done = done;
    }

    public MovementBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public MovementBuilder withSync(boolean sync) {
        this.sync = sync;
        return this;
    }

    public MovementBuilder withTimestamps(int createdAt, int updatedAt) {
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        return this;
    }

    public MovementBuilder withCoordinates(String coordinates) {
        this.coordinates = coordinates;
        return this;
    }

    public MovementBuilder withFees(int feeNumber, int payday) {
        this.feeNumber = feeNumber;
        this.payday = payday;
        return this;
    }

    public MovementBuilder withCategory(ICategory category) {
        this.category = category;
        return this;
    }

    public Movement build() {
        return new Movement(this);
    }

    public MovementEntity buildEntity() {
        return new MovementEntity(this);
    }

    public MovementDTO buildDTO() {
        return new MovementDTO(this);
    }
}
