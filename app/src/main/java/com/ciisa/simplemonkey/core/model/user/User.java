package com.ciisa.simplemonkey.core.model.user;

import java.util.Date;

public class User implements IUser {
    private String id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private Character gender;
    private Date birth;
    private int country;

    User(UserBuilder builder) {
        this.id = builder.id;
        this.email = builder.email;
        this.password = builder.password;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.gender = builder.gender;
        this.birth = builder.birth;
        this.country = builder.country;
    }

    User(IUser user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.gender = user.getGender();
        this.birth = user.getBirth();
        this.country = user.getCountry();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public Character getGender() {
        return gender;
    }

    @Override
    public Date getBirth() {
        return birth;
    }

    @Override
    public int getCountry() {
        return country;
    }
}
