package com.ciisa.simplemonkey.core.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.ciisa.simplemonkey.activities.accountLogin.AccountLogin;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AccessToken {
    public static void refresh(Context context, SharedPreferences sharedPreferences) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            user.getIdToken(true).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
                    String tokenResult = task.getResult().getToken();
                    Log.d("ACCESS_TOKEN", tokenResult);
                    preferencesEditor.putString("token", tokenResult);
                    preferencesEditor.apply();
                } else {
                    mAuth.signOut();
                    SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
                    preferencesEditor.clear();
                    preferencesEditor.apply();
                    Intent intent = new Intent(context, AccountLogin.class);
                    context.startActivity(intent);
                    Toast.makeText(context, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

