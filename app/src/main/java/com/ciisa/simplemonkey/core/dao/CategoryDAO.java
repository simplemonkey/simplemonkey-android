package com.ciisa.simplemonkey.core.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ciisa.simplemonkey.BuildConfig;
import com.ciisa.simplemonkey.core.controller.AdminSQLiteOpenHelper;
import com.ciisa.simplemonkey.core.model.category.CategoryBuilder;
import com.ciisa.simplemonkey.core.model.category.CategoryEntity;

import java.util.ArrayList;

public class CategoryDAO implements InterfaceDAO<CategoryEntity, Integer> {

    private Context context;
    private AdminSQLiteOpenHelper admin;
    private String dbName = BuildConfig.DB_NAME;
    private String tableName = "category";

    public CategoryDAO(Context context) {
        this.context = context;
    }

    @Override
    public CategoryEntity insert(CategoryEntity category) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        ContentValues registry = new ContentValues();

        registry.put("id", category.getId());
        registry.put("name", category.getName());
        registry.put("description", category.getDescription());
        registry.put("color", category.getColor());
        registry.put("icon", category.getIcon());
        registry.put("expense", category.isExpense());
        registry.put("income", category.isIncome());

        if(database.insert(tableName, null, registry) != -1) {
            database.close();
            return category;
        } else {
            database.close();
            return null;
        }
    }

    public boolean saveAll(ArrayList<CategoryEntity> categories) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        database.beginTransaction();
        try {
            for(CategoryEntity category : categories) {
                ContentValues registry = new ContentValues();

                registry.put("id", category.getId());
                registry.put("name", category.getName());
                registry.put("description", category.getDescription());
                registry.put("color", category.getColor());
                registry.put("icon", category.getIcon());
                registry.put("expense", category.isExpense());
                registry.put("income", category.isIncome());

                database.insertWithOnConflict(tableName, null, registry, SQLiteDatabase.CONFLICT_REPLACE);
            }
            database.setTransactionSuccessful();
            database.endTransaction();
            return true;
        } catch (Exception e) {
            Log.w("CategoryDAO Error on saveAll:", e);
            database.endTransaction();
            return false;
        }
    }

    @Override
    public ArrayList<CategoryEntity> findAll(int limit) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        Cursor rows = database.rawQuery("SELECT id, name, description, color, icon, expense, income FROM " + tableName + " ORDER BY id DESC LIMIT ?", new String[]{Integer.toString(limit)});

        ArrayList<CategoryEntity> resultList = new ArrayList<>();

        while(rows.moveToNext()) {
            int id = Integer.parseInt(rows.getString(0));
            String name = rows.getString(1);
            String description = rows.getString(2);
            String color = rows.getString(3);
            String icon = rows.getString(4);
            Boolean expense = rows.getString(5).equals("1");
            Boolean income = rows.getString(6).equals("1");

            CategoryEntity evaluation = new CategoryBuilder(name, description, color, icon)
                    .withId(id)
                    .withConditionals(expense, income)
                    .buildEntity();
            resultList.add(evaluation);
        }
        database.close();
        return resultList;
    }

    public ArrayList<CategoryEntity> findAllWithMovements(String uid) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        Cursor rows = database.rawQuery("SELECT\n" +
                "       c.id, c.name, c.description, c.color, c.icon, c.expense, c.income,\n" +
                "       IFNULL((SELECT SUM(m.amount) FROM movement WHERE c.id = m.id_category AND m.income = 0), 0) AS 'expenses',\n" +
                "       IFNULL((SELECT SUM(m.amount) FROM movement WHERE c.id = m.id_category AND m.income = 1), 0) AS 'incomes'\n" +
                "FROM category c\n" +
                "LEFT JOIN movement m on c.id = m.id_category\n" +
                "WHERE m.uid = ?\n" +
                "GROUP BY c.id", new String[]{uid});

        ArrayList<CategoryEntity> resultList = new ArrayList<>();

        while(rows.moveToNext()) {
            int id = Integer.parseInt(rows.getString(0));
            String name = rows.getString(1);
            String description = rows.getString(2);
            String color = rows.getString(3);
            String icon = rows.getString(4);
            Boolean expense = rows.getString(5).equals("1");
            Boolean income = rows.getString(6).equals("1");
            double totalExpenses = Double.parseDouble(rows.getString(7));
            double totalIncomes = Double.parseDouble(rows.getString(8));

            CategoryEntity category = new CategoryBuilder(name, description, color, icon)
                    .withId(id)
                    .withConditionals(expense, income)
                    .withMovements(totalExpenses, totalIncomes)
                    .buildEntity();

            resultList.add(category);
        }
        database.close();
        return resultList;
    }

    @Override
    public CategoryEntity findById(Integer id) {
        return null;
    }

    @Override
    public boolean update(CategoryEntity category) {
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }
}
