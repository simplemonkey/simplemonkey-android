package com.ciisa.simplemonkey.core.di.modules;

import com.ciisa.simplemonkey.core.controller.RetrofitClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class RetrofitModule {
    @Singleton
    @Provides
    Retrofit provideRetrofitClient() {
        return RetrofitClient.getRetrofitInstance();
    }
}
