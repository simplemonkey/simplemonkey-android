package com.ciisa.simplemonkey.core.model.category;

public class CategoryEntity implements ICategory {
    private int id;
    private String name;
    private String description;
    private String color;
    private String icon;
    private boolean expense;
    private boolean income;
    private double totalExpenses;
    private double totalIncomes;

    CategoryEntity(CategoryBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.color = builder.color;
        this.icon = builder.icon;
        this.expense = builder.expense;
        this.income = builder.income;
        this.totalExpenses = builder.totalExpenses;
        this.totalIncomes = builder.totalIncomes;
    }

    CategoryEntity(ICategory category) {
        this.id = category.getId();
        this.name = category.getName();
        this.description = category.getDescription();
        this.color = category.getColor();
        this.icon = category.getIcon();
        this.expense = category.isExpense();
        this.income = category.isIncome();
        this.totalExpenses = category.getTotalExpenses();
        this.totalIncomes = category.getTotalIncomes();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public boolean isExpense() {
        return expense;
    }

    @Override
    public boolean isIncome() {
        return income;
    }

    @Override
    public double getTotalExpenses() {
        return totalExpenses;
    }

    @Override
    public double getTotalIncomes() {
        return totalIncomes;
    }

    @Override
    public String toString() {
        return "CategoryEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", color='" + color + '\'' +
                ", icon='" + icon + '\'' +
                ", expense=" + expense +
                ", income=" + income +
                ", totalExpenses=" + totalExpenses +
                ", totalIncomes=" + totalIncomes +
                '}';
    }
}
