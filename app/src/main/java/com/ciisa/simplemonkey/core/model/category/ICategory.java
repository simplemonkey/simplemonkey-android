package com.ciisa.simplemonkey.core.model.category;

public interface ICategory<T> {

    int getId();

    String getName();

    String getDescription();

    String getColor();

    String getIcon();

    default boolean isExpense() {
        return false;
    }

    default boolean isIncome() {
        return false;
    }

    default double getTotalExpenses() {
        return 0;
    }

    default double getTotalIncomes() {
        return 0;
    }
}
