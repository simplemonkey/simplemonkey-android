package com.ciisa.simplemonkey.core.model.movement;

import com.ciisa.simplemonkey.core.model.category.ICategory;

import java.util.Date;

public interface IMovement<TCategory extends ICategory> {
    String getId();

    String getUid();

    String getName();

    String getDescription();

    String getDate();

    double getAmount();

    String getCurrency();

    boolean isIncome();

    boolean isDone();

    boolean isSync();

    int getCreatedAt();

    int getUpdatedAt();

    String getCoordinates();

    int getFeeNumber();

    int getPayday();

    TCategory getCategory();
}
