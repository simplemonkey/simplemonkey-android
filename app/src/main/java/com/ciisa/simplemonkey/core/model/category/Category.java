package com.ciisa.simplemonkey.core.model.category;

public class Category implements ICategory {
    protected int id;
    protected String name;
    protected String description;
    protected String color;
    protected String icon;
    protected boolean expense;
    protected boolean income;
    protected double totalExpenses;
    protected double totalIncomes;

    Category(CategoryBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.color = builder.color;
        this.icon = builder.icon;
        this.expense = builder.expense;
        this.income = builder.income;
        this.totalExpenses = builder.totalExpenses;
        this.totalIncomes = builder.totalIncomes;
    }

    Category(ICategory category) {
        this.id = category.getId();
        this.name = category.getName();
        this.description = category.getDescription();
        this.color = category.getColor();
        this.icon = category.getIcon();
        this.expense = category.isExpense();
        this.income = category.isIncome();
        this.totalExpenses = category.getTotalExpenses();
        this.totalIncomes = category.getTotalIncomes();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public boolean isExpense() {
        return expense;
    }

    @Override
    public boolean isIncome() {
        return income;
    }

    @Override
    public double getTotalExpenses() {
        return totalExpenses;
    }

    @Override
    public double getTotalIncomes() {
        return totalIncomes;
    }
}
