package com.ciisa.simplemonkey.core.model.category;

import java.text.NumberFormat;

public class CategoryViewModel extends Category {
    CategoryViewModel(CategoryBuilder builder) {
        super(builder);
    }

    CategoryViewModel(ICategory category) {
        super(category);
    }

    public String getStringExpenses() {
        try {
            return NumberFormat.getInstance().format(super.totalExpenses + " CLP");
        } catch (Exception e){
            return "0 CLP";
        }
    }

    public String getStringIncomes() {
        try {
            return NumberFormat.getInstance().format(super.totalIncomes + " CLP");
        } catch (Exception e){
            return "0 CLP";
        }
    }

    @Override
    public String toString() {
        return "CategoryView{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", color='" + color + '\'' +
                ", icon='" + icon + '\'' +
                ", expense=" + expense +
                ", income=" + income +
                ", totalExpenses=" + totalExpenses +
                ", totalIncomes=" + totalIncomes +
                '}';
    }
}
