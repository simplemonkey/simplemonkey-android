package com.ciisa.simplemonkey.core.model.user;

public class UserMapper {

    /*========= Entity ==========*/

    public static User toBase(IUser user) {
        return new User(user);
    }

    /*========= DTO ==========*/

    public static UserDTO toDTO(IUser user) {
        return new UserDTO(user);
    }

    /*========= ViewModel ==========*/

    public static UserViewModel toViewModel(IUser user) {
        return new UserViewModel(user);
    }
}

