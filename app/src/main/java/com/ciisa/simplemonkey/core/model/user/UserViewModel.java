package com.ciisa.simplemonkey.core.model.user;

public class UserViewModel extends User {
    UserViewModel(UserBuilder builder) {
        super(builder);
    }

    UserViewModel(IUser user) {
        super(user);
    }
}
