package com.ciisa.simplemonkey.core.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ciisa.simplemonkey.BuildConfig;
import com.ciisa.simplemonkey.core.controller.AdminSQLiteOpenHelper;
import com.ciisa.simplemonkey.core.model.category.Category;
import com.ciisa.simplemonkey.core.model.category.CategoryBuilder;
import com.ciisa.simplemonkey.core.model.movement.MovementBuilder;
import com.ciisa.simplemonkey.core.model.movement.MovementEntity;
import com.ciisa.simplemonkey.core.utils.DateConvert;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class MovementDAO implements InterfaceDAO<MovementEntity, String> {

    final private String TABLE_NAME = "movement";

    private Context context;
    private String uid;
    private AdminSQLiteOpenHelper admin;
    private String dbName = BuildConfig.DB_NAME;

    public MovementDAO(Context context, String uid) {
        this.context = context;
        this.uid = uid;
    }

    @Override
    public MovementEntity insert(MovementEntity movement) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        ContentValues registry = new ContentValues();

        if (movement.getId() == null) {
            movement.setId(UUID.randomUUID().toString());
        }

        registry.put("id", movement.getId());
        registry.put("uid", movement.getUid());
        registry.put("name", movement.getName());
        registry.put("description", movement.getDescription());
        registry.put("date", movement.getDate());
        registry.put("amount", movement.getAmount());
        registry.put("currency", movement.getCurrency());
        registry.put("sync", movement.isSync());
        registry.put("coordinates", movement.getCoordinates());
        registry.put("payday", movement.getPayday());
        registry.put("fee_number", movement.getFeeNumber());
        registry.put("income", movement.isIncome());
        registry.put("id_category", movement.getCategory().getId());

        long inserted = database.insert(TABLE_NAME, null, registry);

        Log.d("inserted!", Long.toString(inserted));

        database.close();

        if (inserted != -1) {
            SQLiteDatabase db = admin.getReadableDatabase();
            Cursor row = db.rawQuery("SELECT " +
                            "m.created_at, m.updated_at " +
                            "FROM movement m " +
                            "WHERE m.uid = ? AND m.id = ?",
                    new String[] {uid, movement.getId()});

            if (row.moveToFirst()) {
                movement.setCreatedAt(row.getInt(0));
                movement.setUpdatedAt(row.getInt(1));
            }
            db.close();
            return movement;
        }

        return null;
    }

    public boolean exists (String id) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        Cursor rows = database.rawQuery("SELECT " +
                        "id " +
                        "FROM movement " +
                        "WHERE uid = ? AND id = ?",
                new String[] {uid, id});
        boolean idExists = rows.getCount() > 0;
        database.close();
        return idExists;
    }

    @Override
    public ArrayList<MovementEntity> findAll(int limit) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        Cursor rows = database.rawQuery("SELECT " +
                        "m.id, m.uid, m.name, m.description, m.date, m.amount, m.currency, m.fee_number, m.payday, m.done, m.sync, m.income, m.created_at, m.updated_at, c.id, c.name, c.description, c.color, c.icon " +
                        "FROM movement m " +
                        "LEFT JOIN category c " +
                        "ON m.id_category = c.id " +
                        "WHERE uid = ? " +
                        "ORDER BY m.date DESC, m.created_at DESC " +
                        "LIMIT ?",
                    new String[] {uid, Integer.toString(limit)});

        ArrayList<MovementEntity> resultList = new ArrayList<>();

        while(rows.moveToNext()) {
            String id = rows.getString(0);
            String uid = rows.getString(1);
            String name = rows.getString(2);
            String description = rows.getString(3);
            String date = rows.getString(4);
            Double amount = Double.parseDouble(rows.getString(5));
            String currency = rows.getString(6);
            int feeNumber = Integer.parseInt(rows.getString(7));
            int payday = Integer.parseInt(rows.getString(8));
            boolean done = "1".equals(rows.getString(9));
            boolean sync = "1".equals(rows.getString(10));
            boolean income = "1".equals(rows.getString(11));
            int createdAt = Integer.parseInt(rows.getString(12));
            int updatedAt = Integer.parseInt(rows.getString(13));
            int categoryId = Integer.parseInt(rows.getString(14));
            String categoryName = rows.getString(15);
            String categoryDesc = rows.getString(16);
            String categoryColor = rows.getString(17);
            String categoryIcon = rows.getString(18);

            Category category = new CategoryBuilder(categoryName, categoryDesc, categoryColor, categoryIcon)
                    .withId(categoryId)
                    .build();

            MovementEntity movement = new MovementBuilder(uid, name, description, date, amount, currency, income, done)
                    .withId(id)
                    .withFees(feeNumber, payday)
                    .withCategory(category)
                    .withSync(sync)
                    .withTimestamps(createdAt, updatedAt)
                    .buildEntity();

            resultList.add(movement);
        }
        database.close();
        return resultList;
    }

    @Override
    public MovementEntity findById(String id) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        Cursor row = database.rawQuery("SELECT " +
                        "m.name, m.description, m.date, m.amount, m.currency, m.fee_number, m.payday, m.done, m.sync, m.income, m.created_at, m.updated_at, c.id, c.name, c.description, c.color, c.icon " +
                        "FROM movement m " +
                        "LEFT JOIN category c " +
                        "ON m.id_category = c.id " +
                        "WHERE m.uid = ? AND m.id = ?",
                new String[] {uid, id});

        if(row.moveToFirst()) {
            String name = row.getString(0);
            String description = row.getString(1);
            String date = row.getString(2);
            Double amount = Double.parseDouble(row.getString(3));
            String currency = row.getString(4);
            int feeNumber = Integer.parseInt(row.getString(5));
            int payday = Integer.parseInt(row.getString(6));
            boolean done = "1".equals(row.getString(7));
            boolean sync = "1".equals(row.getString(8));
            boolean income = "1".equals(row.getString(9));
            int createdAt = Integer.parseInt(row.getString(10));
            int updatedAt = Integer.parseInt(row.getString(11));
            int categoryId = Integer.parseInt(row.getString(12));
            String categoryName = row.getString(13);
            String categoryDesc = row.getString(14);
            String categoryColor = row.getString(15);
            String categoryIcon = row.getString(16);

            database.close();

            Category category = new CategoryBuilder(categoryName, categoryDesc, categoryColor, categoryIcon)
                    .withId(categoryId)
                    .build();

            MovementEntity movement = new MovementBuilder(uid, name, description, date, amount, currency, income, done)
                    .withId(id)
                    .withFees(feeNumber, payday)
                    .withCategory(category)
                    .withSync(sync)
                    .withTimestamps(createdAt, updatedAt)
                    .buildEntity();

            return movement;
        } else {
            database.close();
            return null;
        }
    }

    @Override
    public boolean update(MovementEntity movement) {
        Log.d("updatemovement", movement.toString());
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        ContentValues registry = new ContentValues();

        registry.put("uid", movement.getUid());
        registry.put("name", movement.getName());
        registry.put("description", movement.getDescription());
        registry.put("date", movement.getDate());
        registry.put("amount", movement.getAmount());
        registry.put("currency", movement.getCurrency());
        registry.put("sync", movement.isSync());
        registry.put("coordinates", movement.getCoordinates());
        registry.put("payday", movement.getPayday());
        registry.put("fee_number", movement.getFeeNumber());
        registry.put("income", movement.isIncome());
        registry.put("id_category", movement.getCategory().getId());

        boolean isUpdated = database.update(TABLE_NAME, registry, "id = ?", new String[] {movement.getId()}) > 0;

        database.close();
        if(isUpdated) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteById(String id) {
        admin = new AdminSQLiteOpenHelper(context, dbName, null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();

        int qty = database.delete(TABLE_NAME, "id = ?" ,  new String[] {id});
        return qty > 0 ;
    }
}
