package com.ciisa.simplemonkey.core.di.modules;

import com.ciisa.simplemonkey.core.service.CategoryService;
import com.ciisa.simplemonkey.core.service.MovementService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(
    includes = RetrofitModule.class
)
public class ServicesModule {
    @Provides
    CategoryService provideCategoryService (Retrofit retrofit) {
        return retrofit.create(CategoryService.class);
    }

    @Provides
    MovementService provideMovementService (Retrofit retrofit) {
        return retrofit.create(MovementService.class);
    }
}
