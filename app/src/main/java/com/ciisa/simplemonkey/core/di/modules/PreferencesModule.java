package com.ciisa.simplemonkey.core.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {
    @Singleton
    @Provides
    public SharedPreferences providesSharedPreferences(Context context) {
        return context.getSharedPreferences("userData", Context.MODE_PRIVATE);
    }
}
