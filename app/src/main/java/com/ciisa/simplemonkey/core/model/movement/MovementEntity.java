package com.ciisa.simplemonkey.core.model.movement;

import com.ciisa.simplemonkey.core.model.category.CategoryEntity;
import com.ciisa.simplemonkey.core.model.category.CategoryMapper;
import com.ciisa.simplemonkey.core.utils.DateConvert;

import java.util.Date;

public class MovementEntity implements IMovement<CategoryEntity> {
    private String id;
    private String uid;
    private String name;
    private String description;
    private String date;
    private double amount;
    private String currency;
    private boolean income;
    private boolean done;
    private boolean sync;
    private int createdAt;
    private int updatedAt;
    private String coordinates;
    private int feeNumber;
    private int payday;
    private CategoryEntity category;

    MovementEntity(MovementBuilder builder) {
        this.id = builder.id;
        this.uid = builder.uid;
        this.name = builder.name;
        this.description = builder.description;
        this.date = builder.date;
        this.amount = builder.amount;
        this.currency = builder.currency;
        this.income = builder.income;
        this.done = builder.done;
        this.sync = builder.sync;
        this.createdAt = builder.createdAt;
        this.updatedAt = builder.updatedAt;
        this.coordinates = builder.coordinates;
        this.feeNumber = builder.feeNumber;
        this.payday = builder.payday;
        this.category = CategoryMapper.toEntity(builder.category);
    }

    MovementEntity(IMovement movement) {
        this.id = movement.getId();
        this.uid = movement.getUid();
        this.name = movement.getName();
        this.description = movement.getDescription();
        this.date = movement.getDate();
        this.amount = movement.getAmount();
        this.currency = movement.getCurrency();
        this.income = movement.isIncome();
        this.done = movement.isDone();
        this.sync = movement.isSync();
        this.createdAt = movement.getCreatedAt();
        this.updatedAt = movement.getUpdatedAt();
        this.coordinates = movement.getCoordinates();
        this.feeNumber = movement.getFeeNumber();
        this.payday = movement.getPayday();
        this.category = CategoryMapper.toEntity(movement.getCategory());
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getUid() {
        return uid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public boolean isIncome() {
        return income;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public boolean isSync() {
        return sync;
    }

    @Override
    public int getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(int createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(int updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String getCoordinates() {
        return coordinates;
    }

    @Override
    public int getFeeNumber() {
        return feeNumber;
    }

    @Override
    public int getPayday() {
        return payday;
    }

    @Override
    public CategoryEntity getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "MovementEntity{" +
                "id='" + id + '\'' +
                ", uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", income=" + income +
                ", done=" + done +
                ", sync=" + sync +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", coordinates='" + coordinates + '\'' +
                ", feeNumber=" + feeNumber +
                ", payday=" + payday +
                ", category=" + category +
                '}';
    }
}
