package com.ciisa.simplemonkey.core.di;

import android.content.Context;

import com.ciisa.simplemonkey.appContext.SimpleMonkeyApp;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 * Los modulos pueden ser clases normal abstractas o interfaces, en el caso de las interfaces y clases abstractas Dagger se encarga contruir la concrecion
 * para este tipo de modulos (interface/abstract class) se pueden utilizar para hacer Binds (Enlaces)
 */
@Module
public interface SimpleMonkeyModule {
    @Singleton
    @Binds
    Context bindContext(SimpleMonkeyApp app);
}
