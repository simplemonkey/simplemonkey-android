package com.ciisa.simplemonkey.core.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.ciisa.simplemonkey.core.dao.CalculationDAO;
import com.ciisa.simplemonkey.core.dao.CategoryDAO;
import com.ciisa.simplemonkey.core.dao.MovementDAO;
import com.ciisa.simplemonkey.core.dao.MovementSyncDAO;

import dagger.Module;
import dagger.Provides;

@Module(
    includes = PreferencesModule.class
)
public class DAOModule {
    @Provides
    CategoryDAO provideCategoryDAO(Context context) {
        return new CategoryDAO(context);
    }

    @Provides
    MovementDAO provideMovementDAO(Context context, SharedPreferences preferences) {
        String uid = preferences.getString("uid", "default");
        return new MovementDAO(context, uid);
    }

    @Provides
    MovementSyncDAO provideMovementSyncDAO(Context context, SharedPreferences preferences) {
        String uid = preferences.getString("uid", "default");
        return new MovementSyncDAO(context, uid);
    }

    @Provides
    CalculationDAO provideCalculationDAO(Context context, SharedPreferences preferences) {
        String uid = preferences.getString("uid", "default");
        return new CalculationDAO(context, uid);
    }
}