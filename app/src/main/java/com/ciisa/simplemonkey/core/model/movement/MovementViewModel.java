package com.ciisa.simplemonkey.core.model.movement;

import com.ciisa.simplemonkey.core.utils.DateConvert;

import java.text.NumberFormat;

public class MovementViewModel extends Movement {
    MovementViewModel(MovementBuilder builder) {
        super(builder);
    }

    MovementViewModel(IMovement movement) {
        super(movement);
    }

    public String getAmountToString() {
        return NumberFormat.getInstance().format(this.amount + " " + this.currency);
    }

}
