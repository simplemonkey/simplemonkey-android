package com.ciisa.simplemonkey.core.model.user;

import java.util.Date;

public class UserBuilder {
    String id;
    String email;
    String password;
    String firstName;
    String lastName;
    Character gender;
    Date birth;
    int country;

    public UserBuilder(String email, String firstName, String lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserBuilder withBuilder(String id) {
        this.id = id;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withGender(Character gender) {
        this.gender = gender;
        return this;
    }

    public UserBuilder withBirth(Date birth) {
        this.birth = birth;
        return this;
    }

    public UserBuilder withCountry(int country) {
        this.country = country;
        return this;
    }

    public User build() {
        return new User(this);
    }

    public UserDTO buildDTO() {
        return new UserDTO(this);
    }

    public UserViewModel buildViewModel() {
        return new UserViewModel(this);
    }
}
