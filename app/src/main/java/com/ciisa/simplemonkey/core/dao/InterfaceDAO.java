package com.ciisa.simplemonkey.core.dao;

import java.util.ArrayList;

interface InterfaceDAO<TEntity, TId> {
    TEntity insert(TEntity t);

    ArrayList<TEntity> findAll(int limit);

    TEntity findById(TId id);

    boolean update(TEntity t);

    boolean deleteById(TId id);
}