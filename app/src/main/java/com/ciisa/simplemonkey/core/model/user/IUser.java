package com.ciisa.simplemonkey.core.model.user;

import java.util.Date;

public interface IUser {
    String getId();

    String getEmail();

    String getPassword();

    String getFirstName();

    String getLastName();

    Character getGender();

    Date getBirth();

    default int getCountry() {
        return 0;
    }
}
