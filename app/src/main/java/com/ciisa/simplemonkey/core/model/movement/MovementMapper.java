package com.ciisa.simplemonkey.core.model.movement;

import com.ciisa.simplemonkey.core.model.category.Category;
import com.ciisa.simplemonkey.core.model.category.ICategory;

import java.util.ArrayList;
import java.util.List;

public class MovementMapper {
    /* ========== Base ======== */

    public static Movement toBase(IMovement movement) {
        return movement != null ? new Movement(movement) : null;
    }

    public static ArrayList<Movement> toBase(List<IMovement> iMovements) {
        final ArrayList<Movement> baseMovements = new ArrayList<>();
        if (iMovements != null) {
            for (IMovement movement : iMovements) {
                Movement convertedMovement = toBase(movement);
                baseMovements.add(convertedMovement);
            }
        }
        return baseMovements;
    }

    /* ========== Entity ======== */

    public static MovementEntity toEntity(IMovement movement) {
        return movement != null ? new MovementEntity(movement) : null;

    }

    public static ArrayList<MovementEntity> toEntity(List<IMovement> iMovements) {
        final ArrayList<MovementEntity> entityMovements = new ArrayList<>();
        if (iMovements != null) {
            for (IMovement movement : iMovements) {
                MovementEntity convertedMovement = toEntity(movement);
                entityMovements.add(convertedMovement);
            }
        }
        return entityMovements;
    }

    /* ========== DTO ======== */

    public static MovementDTO toDTO(IMovement movement) {
        return movement != null ? new MovementDTO(movement) : null;

    }

    public static ArrayList<MovementDTO> toDTO(List<IMovement> iMovements) {
        final ArrayList<MovementDTO> DTOMovements = new ArrayList<>();
        if (iMovements != null) {
            for (IMovement movement : iMovements) {
                MovementDTO convertedMovement = toDTO(movement);
                DTOMovements.add(convertedMovement);
            }
        }
        return DTOMovements;
    }

    /* ========== ViewModel ======== */

    public static MovementViewModel toViewModel(IMovement movement) {
        return movement != null ? new MovementViewModel(movement) : null;
    }

    public static ArrayList<MovementViewModel> toViewModel(List<MovementEntity> iMovements) {
        final ArrayList<MovementViewModel> viewModelsMovements = new ArrayList<>();
        if (iMovements != null) {
            for (IMovement movement : iMovements) {
                MovementViewModel convertedMovement = toViewModel(movement);
                viewModelsMovements.add(convertedMovement);
            }
        }
        return viewModelsMovements;
    }
}
