package com.ciisa.simplemonkey.core.model.movement;

import com.ciisa.simplemonkey.core.model.category.Category;
import com.ciisa.simplemonkey.core.model.category.CategoryMapper;

import java.util.Date;

public class Movement implements IMovement<Category> {
    protected String id;
    protected String uid;
    protected String name;
    protected String description;
    protected String date;
    protected double amount;
    protected String currency;
    protected boolean income;
    protected boolean done;
    protected boolean sync;
    protected int createdAt;
    protected int updatedAt;
    protected String coordinates;
    protected int feeNumber;
    protected int payday;
    protected Category category;

    Movement(MovementBuilder builder) {
        this.id = builder.id;
        this.uid = builder.uid;
        this.name = builder.name;
        this.description = builder.description;
        this.date = builder.date;
        this.amount = builder.amount;
        this.currency = builder.currency;
        this.income = builder.income;
        this.done = builder.done;
        this.sync = builder.sync;
        this.createdAt = builder.createdAt;
        this.updatedAt = builder.updatedAt;
        this.coordinates = builder.coordinates;
        this.feeNumber = builder.feeNumber;
        this.payday = builder.payday;
        this.category = CategoryMapper.toBase(builder.category);
    }

    Movement(IMovement movement) {
        this.id = movement.getId();
        this.uid = movement.getUid();
        this.name = movement.getName();
        this.description = movement.getDescription();
        this.date = movement.getDate();
        this.amount = movement.getAmount();
        this.currency = movement.getCurrency();
        this.income = movement.isIncome();
        this.done = movement.isDone();
        this.sync = movement.isSync();
        this.createdAt = movement.getCreatedAt();
        this.updatedAt = movement.getUpdatedAt();
        this.coordinates = movement.getCoordinates();
        this.feeNumber = movement.getFeeNumber();
        this.payday = movement.getPayday();
        this.category = CategoryMapper.toBase(movement.getCategory());
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getUid() {
        return uid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public boolean isIncome() {
        return income;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public boolean isSync() {
        return sync;
    }

    @Override
    public int getCreatedAt() {
        return createdAt;
    }

    @Override
    public int getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String getCoordinates() {
        return coordinates;
    }

    @Override
    public int getFeeNumber() {
        return feeNumber;
    }

    @Override
    public int getPayday() {
        return payday;
    }

    @Override
    public Category getCategory() {
        return category;
    }
}
