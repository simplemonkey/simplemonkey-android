package com.ciisa.simplemonkey.core.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.ciisa.simplemonkey.R;

public class FormError {

    public static void vibrate(Context context){
        MediaPlayer mp3Error;
        mp3Error= MediaPlayer.create(context.getApplicationContext(),R.raw.bip);
        mp3Error.start();
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(300);
    }
}
