package com.ciisa.simplemonkey.core.di;

import com.ciisa.simplemonkey.activities.dashboard.DashboardModule;
import com.ciisa.simplemonkey.activities.movement.MovementModule;
import com.ciisa.simplemonkey.activities.splashScreen.SplashScreenModule;
import com.ciisa.simplemonkey.appContext.SimpleMonkeyApp;
import com.ciisa.simplemonkey.activities.accountLogin.AccountLoginModule;
import com.ciisa.simplemonkey.core.di.modules.DAOModule;
import com.ciisa.simplemonkey.core.di.modules.FirebaseModule;
import com.ciisa.simplemonkey.core.di.modules.PreferencesModule;
import com.ciisa.simplemonkey.core.di.modules.RetrofitModule;
import com.ciisa.simplemonkey.core.di.modules.ServicesModule;
import com.ciisa.simplemonkey.core.di.modules.ValidatorModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

/**
 * Esta clase se encarga de saber como resolver las dependencias que tenga nuestra clase SimpleMonkeyApp
 */
@Singleton
@Component(
        /**
         * Los modulos son los que se encargan de crear las instancias de dependencias
        */
        modules = {
                // Modules
                AndroidInjectionModule.class,
                SimpleMonkeyModule.class,
                PreferencesModule.class,
                ValidatorModule.class,
                FirebaseModule.class,
                RetrofitModule.class,
                DAOModule.class,
                ServicesModule.class,
                // Activities
                AccountLoginModule.class,
                DashboardModule.class,
                SplashScreenModule.class,
                MovementModule.class
        }
)
public interface SimpleMonkeyComponent extends AndroidInjector<SimpleMonkeyApp> {
    @Component.Factory
    interface Factory extends AndroidInjector.Factory<SimpleMonkeyApp> {

    }
}
