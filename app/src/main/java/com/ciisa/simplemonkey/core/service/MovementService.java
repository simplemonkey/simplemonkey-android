package com.ciisa.simplemonkey.core.service;

import com.ciisa.simplemonkey.core.model.movement.MovementDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MovementService {
    @GET("/movements/{uid}")
    Call<List<MovementDTO>> findAll(@Path("uid") String uid, @Header("Authorization") String token);

    @GET("/movements/{uid}/{movementId}")
    Call<MovementDTO> findById(@Path("uid") String uid, @Path("movementId") String movementId, @Header("Authorization") String token);

    @POST("/movements/{uid}")
    Call<Void> insert(@Path("uid") String uid, @Header("Authorization") String token, @Body MovementDTO movement);

    @PUT("/movements/{uid}/{movementId}")
    Call<Void> update(@Path("uid") String uid, @Path("movementId") String movementId, @Header("Authorization") String token, @Body MovementDTO movement);

    @DELETE("/movements/{uid}/{movementId}")
    Call<Void> delete(@Path("uid") String uid, @Path("movementId") String movementId, @Header("Authorization") String token);
}
