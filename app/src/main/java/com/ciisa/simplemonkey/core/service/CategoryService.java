package com.ciisa.simplemonkey.core.service;

import com.ciisa.simplemonkey.core.model.category.CategoryDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoryService {
    @GET("/categories")
    Call<List<CategoryDTO>> findAll();
}
