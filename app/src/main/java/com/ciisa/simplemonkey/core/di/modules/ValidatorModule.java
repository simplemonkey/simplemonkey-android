package com.ciisa.simplemonkey.core.di.modules;

import android.content.Context;

import com.ciisa.simplemonkey.core.utils.InputValidator;

import dagger.Module;
import dagger.Provides;

@Module
public class ValidatorModule {
    @Provides
    InputValidator providesInputValidator(Context context) {
        return new InputValidator(context);
    }
}
