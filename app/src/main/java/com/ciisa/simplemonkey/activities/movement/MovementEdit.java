package com.ciisa.simplemonkey.activities.movement;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.core.adapter.CategoryAdapter;
import com.ciisa.simplemonkey.core.dao.CategoryDAO;
import com.ciisa.simplemonkey.core.dao.MovementDAO;
import com.ciisa.simplemonkey.core.dao.MovementSyncDAO;
import com.ciisa.simplemonkey.core.model.category.Category;
import com.ciisa.simplemonkey.core.model.category.CategoryBuilder;
import com.ciisa.simplemonkey.core.model.category.CategoryMapper;
import com.ciisa.simplemonkey.core.model.movement.MovementBuilder;
import com.ciisa.simplemonkey.core.model.movement.MovementEntity;
import com.ciisa.simplemonkey.core.model.movement.MovementMapper;
import com.ciisa.simplemonkey.core.model.movement.MovementViewModel;
import com.ciisa.simplemonkey.core.service.MovementService;
import com.ciisa.simplemonkey.core.ui.DatePickerFragment;
import com.ciisa.simplemonkey.core.utils.DateConvert;
import com.ciisa.simplemonkey.core.utils.FormError;
import com.ciisa.simplemonkey.core.utils.InputValidator;
import com.google.android.material.textfield.TextInputLayout;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MovementEdit extends AppCompatActivity implements HasAndroidInjector {

    @Inject DispatchingAndroidInjector<Object> androidInjector;
    @Inject SharedPreferences preferences;
    @Inject MovementService movementService;
    @Inject CategoryDAO categoryDAO;
    @Inject MovementDAO movementDAO;
    @Inject MovementSyncDAO movementSyncDAO;

    private TextInputLayout tilName, tilAmount, tilDate, tilDescription;
    private Spinner spType, spCategory;
    private Button btnRegister;
    private String id, uid, token;
    private MovementViewModel movement;
    private CategoryAdapter categoryAdapter;
    private ArrayAdapter<String> movementTypes;
    private String[] typesArray;

    private void showDatePickerDialog(final TextInputLayout til) {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                final String selectedDate = String.format("%d-%02d-%02d", year, (month +1), day);
                til.getEditText().setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_edit);
        AndroidInjection.inject(this);

        btnRegister = findViewById(R.id.btnRegister);
        tilName = findViewById(R.id.tilName);
        tilAmount = findViewById(R.id.tilAmount);
        tilDate = findViewById(R.id.tilDate);
        tilDescription = findViewById(R.id.tilDescription);
        spType = findViewById(R.id.spType);
        spCategory = findViewById(R.id.spCategory);

        id = getIntent().getStringExtra("id");
        uid = preferences.getString("uid", "");
        token = preferences.getString("token", "");

        movement = MovementMapper.toViewModel(movementDAO.findById(id));

        tilDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(tilDate);
            }
        });

        categoryAdapter = new CategoryAdapter(
                this,
                CategoryMapper.toViewModel(categoryDAO.findAll(100))
        );

        typesArray =  getResources().getStringArray(R.array.movement_types);
        movementTypes = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, typesArray);

        if(movement != null) {
            tilName.getEditText().setText(movement.getName());
            tilAmount.getEditText().setText(String.format("%.0f",movement.getAmount()));
            tilDate.getEditText().setText(movement.getDate());
            tilDescription.getEditText().setText(movement.getDescription());
            spCategory.setAdapter(categoryAdapter);
            spType.setAdapter(movementTypes);
        } else {
            Toast.makeText(MovementEdit.this,"Falló el intent", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(MovementEdit.this, MovementList.class);
            startActivity(intent);
            finish();
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = tilName.getEditText().getText().toString();
                String amount = tilAmount.getEditText().getText().toString();
                String date = tilDate.getEditText().getText().toString();
                String description = tilDescription.getEditText().getText().toString();
                boolean income = isIncome((String) spType.getSelectedItem());
                int categoryId = (int) spCategory.getSelectedItemId();

                InputValidator inputValidator = new InputValidator(v.getContext());

                inputValidator.isRequired(tilName);
                inputValidator.isNumber(tilAmount);
                inputValidator.isRequired(tilAmount);
                inputValidator.isRequired(tilDate);
                inputValidator.isRequired(tilDescription);

                if(inputValidator.validate()) {
                    Category category = new CategoryBuilder()
                            .withId(categoryId)
                            .build();

                    MovementEntity movement = new MovementBuilder(
                            uid,
                            name,
                            description,
                            date,
                            Double.parseDouble(amount),
                            "CLP",
                            income,
                            true
                    )
                            .withId(id)
                            .withCategory(category)
                            .buildEntity();

                    if(movementDAO.update(movement)) {
                        Call<Void> movementCall = movementService.update(uid, movement.getId(), token, MovementMapper.toDTO(movement));
                        movementCall.enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                if (response.isSuccessful()) {
                                    movementSyncDAO.delete(movement.getId());
                                    Toast.makeText(v.getContext(), "Movimiento sincronizado con éxito", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {

                            }
                        });
                        Toast.makeText(v.getContext(), "Movimiento actualizado con éxito", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getBaseContext(), MovementList.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(v.getContext(), "Error al escribir en sqlite", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    FormError.vibrate(v.getContext());
                    Toast.makeText(v.getContext(), "Campos Erroneos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    boolean isIncome (String type) {
        return type.equals(typesArray[1]);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }
}