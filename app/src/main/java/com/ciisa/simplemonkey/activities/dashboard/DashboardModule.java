package com.ciisa.simplemonkey.activities.dashboard;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class DashboardModule {
    /**
     * Esta metodo nos permite añadir al map de dependencias de dagger android el class AccountLogin
     * @return
     */
    @ContributesAndroidInjector
    abstract Dashboard contributeDashboard();
}
