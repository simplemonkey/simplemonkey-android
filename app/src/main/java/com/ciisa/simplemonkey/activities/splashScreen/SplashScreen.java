package com.ciisa.simplemonkey.activities.splashScreen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.activities.accountLogin.AccountLogin;
import com.ciisa.simplemonkey.core.dao.CategoryDAO;
import com.ciisa.simplemonkey.core.model.category.CategoryDTO;
import com.ciisa.simplemonkey.core.model.category.CategoryMapper;
import com.ciisa.simplemonkey.core.service.CategoryService;
import com.ciisa.simplemonkey.activities.dashboard.Dashboard;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SplashScreen extends AppCompatActivity implements HasAndroidInjector {

    @Inject DispatchingAndroidInjector<Object> androidInjector;
    @Inject SharedPreferences preferences;
    @Inject CategoryService categoryService;
    @Inject CategoryDAO categoryDAO;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);

        setContentView(R.layout.activity_splash_screen);

        final boolean isUserActive = preferences.contains("uid");

        final Call<List<CategoryDTO>> call = categoryService.findAll();

        final Intent intent = new Intent(SplashScreen.this, isUserActive ? Dashboard.class : AccountLogin.class);

        call.enqueue(new Callback<List<CategoryDTO>>() {
            @Override
            public void onResponse(Call<List<CategoryDTO>> call, Response<List<CategoryDTO>> response) {
                if (response.isSuccessful()) {
                    List<CategoryDTO> categories = response.body();
                    for (CategoryDTO category: categories) {
                        Log.d("Categorias desde API", category.toString());
                    }
                    categoryDAO.saveAll(CategoryMapper.toEntity(categories));
                    Toast.makeText(SplashScreen.this, "Sincronización de categorías exitosa", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(SplashScreen.this, "Error en la respuesta. CODE " + response.code(), Toast.LENGTH_LONG).show();
                }
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<List<CategoryDTO>> call, Throwable t) {
                Toast.makeText(SplashScreen.this, "Error al comunicarse con el servicios", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }
}





