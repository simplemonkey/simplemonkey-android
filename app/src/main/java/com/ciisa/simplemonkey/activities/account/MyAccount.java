package com.ciisa.simplemonkey.activities.account;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import com.ciisa.simplemonkey.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MyAccount extends AppCompatActivity {

    private TextView  tvFirstnameLastname,tvBirth,tvEmail;
    private DatabaseReference mDatabase;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        tvFirstnameLastname = findViewById(R.id.tvFirstnameLastname);
        tvBirth = findViewById(R.id.tvBirth);
        tvEmail = findViewById(R.id.tvEmail);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        mDatabase.child("Users").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (dataSnapshot.exists()) {

                    String firstname = dataSnapshot.child("firstName").getValue().toString();
                    String lastname = dataSnapshot.child("lastName").getValue().toString();
                    String birth = dataSnapshot.child("birth").getValue().toString();

                    tvEmail.setText(user.getEmail());
                    tvFirstnameLastname.setText(firstname+" "+lastname);
                    tvBirth.setText(birth);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MyAccount.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
