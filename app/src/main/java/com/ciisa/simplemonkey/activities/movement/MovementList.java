package com.ciisa.simplemonkey.activities.movement;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.activities.dashboard.Dashboard;
import com.ciisa.simplemonkey.core.adapter.MovementAdapter;
import com.ciisa.simplemonkey.core.dao.MovementDAO;
import com.ciisa.simplemonkey.core.model.movement.MovementMapper;
import com.ciisa.simplemonkey.core.model.movement.MovementViewModel;

import java.util.ArrayList;

public class MovementList extends AppCompatActivity {

    private ListView lvViewAll;
    private String uid;
    private ArrayList<MovementViewModel> movements;
    private MovementAdapter movementAdapter;
    private MovementDAO movementDAO;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_list);

        lvViewAll = findViewById(R.id.lvViewAll);

        preferences = getSharedPreferences("userData", Context.MODE_PRIVATE);
        uid = preferences.getString("uid", "");
        movementDAO = new MovementDAO(MovementList.this, uid);
        movements = MovementMapper.toViewModel(movementDAO.findAll(100));
        movementAdapter = new MovementAdapter(MovementList.this, movements);
        lvViewAll.setAdapter(movementAdapter);

        if (movements.isEmpty()) {
            Intent intent = new Intent(MovementList.this, Dashboard.class);
            startActivity(intent);
        }

       lvViewAll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MovementViewModel movement = movements.get(position);
                Intent intent = new Intent(MovementList.this, MovementDetail.class);
                intent.putExtra("id", movement.getId());
                startActivity(intent);
            }
        });
    }

    public void onBackPressed() {
        Intent intent = new Intent(MovementList.this, Dashboard.class);
        startActivity(intent);
    }

}
