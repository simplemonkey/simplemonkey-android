package com.ciisa.simplemonkey.activities.dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ciisa.simplemonkey.activities.account.MyAccount;
import com.ciisa.simplemonkey.activities.movement.MovementCreate;
import com.ciisa.simplemonkey.activities.movement.MovementList;
import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.activities.accountLogin.AccountLogin;
import com.ciisa.simplemonkey.core.adapter.MovementAdapter;
import com.ciisa.simplemonkey.core.dao.CalculationDAO;
import com.ciisa.simplemonkey.core.dao.CategoryDAO;
import com.ciisa.simplemonkey.core.dao.MovementDAO;
import com.ciisa.simplemonkey.core.dao.MovementSyncDAO;
import com.ciisa.simplemonkey.core.model.category.CategoryEntity;
import com.ciisa.simplemonkey.core.model.movement.MovementDTO;
import com.ciisa.simplemonkey.core.model.movement.MovementEntity;
import com.ciisa.simplemonkey.core.model.movement.MovementMapper;
import com.ciisa.simplemonkey.core.service.MovementService;
import com.ciisa.simplemonkey.core.utils.AccessToken;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Dashboard extends AppCompatActivity implements HasAndroidInjector {

    @Inject DispatchingAndroidInjector<Object> androidInjector;
    @Inject SharedPreferences preferences;
    @Inject Retrofit retrofitClient;
    @Inject CategoryDAO categoryDAO;
    @Inject MovementDAO movementDAO;
    @Inject CalculationDAO calculationDAO;
    @Inject MovementSyncDAO movementSyncDAO;

    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;
    private String uid;

    private ListView lvMovements;
    private TextView tvBalance,tvWelcome,tvMessage;
    private Button btnLogout, btnMyAccount;
    private FloatingActionButton fab;
    private LinearLayout llBalance, llMovements, llChart, llNoData;

    private ArrayList<MovementEntity> movements;
    private ArrayList<CategoryEntity> categories;
    private MovementAdapter movementAdapter;

    private FirebaseAuth firebaseAuth;

    private double balance;

    private static String TAG = "MainActivity";

    private List<PieEntry> incomesByCategory = new ArrayList<>();
    private ArrayList<Integer> incomesColors = new ArrayList<>();
    private DatabaseReference mDatabase;

    PieChart pieChart;
    Button btnAllMovements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_dashboard);

        uid = preferences.getString("uid", "test");

        final MovementService service = retrofitClient.create(MovementService.class);

        syncFromCloud(service);

        lvMovements = findViewById(R.id.lvMovements);
        tvBalance = findViewById(R.id.tvBalance);
        tvMessage = findViewById(R.id.tvMessage);
        tvWelcome = findViewById(R.id.tvWelcome);
        btnAllMovements = findViewById(R.id.btnAllMovements);
        btnLogout = findViewById(R.id.btnLogout);
        btnMyAccount = findViewById(R.id.btnMyAccount);
        fab = findViewById(R.id.fab);
        llBalance = findViewById(R.id.llBalance);
        llMovements = findViewById(R.id.llMovements);
        llChart = findViewById(R.id.llChart);
        llNoData = findViewById(R.id.llNoData);


        mDatabase = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        firebaseAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("Users").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (dataSnapshot.exists()) {

                String firstname = dataSnapshot.child("firstName").getValue().toString();
                String welcome = getResources().getString(R.string.welcome, firstname);
                tvWelcome.setText(welcome);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Dashboard.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        movements = movementDAO.findAll(3);
        for(MovementEntity movement: movements) {
            Log.d("Movimientos desde SQLite", movement.toString());
        }

        if (movements.isEmpty()) {
            llNoData.setVisibility(View.VISIBLE);
            llBalance.setVisibility(View.GONE);
            llMovements.setVisibility(View.GONE);
            llChart.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.GONE);
        }

        balance = calculationDAO.getBalance();
        tvBalance.setText(NumberFormat.getInstance().format(balance) + " CLP");
        movementAdapter = new MovementAdapter(Dashboard.this, MovementMapper.toViewModel(movements));
        lvMovements.setAdapter(movementAdapter);


        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                SharedPreferences.Editor preferencesEditor = preferences.edit();
                preferencesEditor.clear();
                preferencesEditor.apply();

                Intent intent = new Intent(Dashboard.this, AccountLogin.class);
                startActivity(intent);
                Toast.makeText(Dashboard.this, "Sesión cerrada con éxito", Toast.LENGTH_SHORT).show();
            }
        });

        btnMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, MyAccount.class);
                startActivity(intent);
            }
        });

        pieChart = (PieChart) findViewById(R.id.piechart);

        pieChart.setRotationEnabled(true);
        //pieChart.setUsePercentValues(true);
        //pieChart.setHoleColor(Color.BLUE);
        //pieChart.setCenterTextColor(Color.BLACK);
        Description chartDescription = new Description();
        chartDescription.setText("");
        pieChart.setDescription(chartDescription);
        pieChart.setHoleRadius(50f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText(getString(R.string.expenses));
        pieChart.setCenterTextSize(16);
        pieChart.setDrawEntryLabels(false);
        //More options just check out the documentation!

        categories = categoryDAO.findAllWithMovements(uid);
        addDataSet(categories);

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.d(TAG, "onValueSelected: Value select from chart.");
                Log.d(TAG, "onValueSelected: " + e.toString());
                Log.d(TAG, "onValueSelected: " + h.toString());
            }

            @Override
            public void onNothingSelected() {

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MovementCreate.class);
                startActivity(intent);
            }
        });

        btnAllMovements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MovementList.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        AccessToken.refresh(getBaseContext(), preferences);
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            finishAffinity();
            System.exit(0);
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.dashboard_press_back), Toast.LENGTH_SHORT).show(); }
        mBackPressed = System.currentTimeMillis();
    }

    private void addDataSet(ArrayList<CategoryEntity> categories) {
        Log.d(TAG, "addDataSet started");
        List<PieEntry> expensesByCategory = new ArrayList<>();
        ArrayList<Integer> expensesColors = new ArrayList<>();

        for (CategoryEntity category : categories) {
            if (category.getTotalExpenses() > 0) {
                expensesByCategory.add(new PieEntry((float) category.getTotalExpenses(), category.getName()));
                expensesColors.add(Color.parseColor(category.getColor()));
            }
            if (category.getTotalIncomes() > 0) {
                incomesByCategory.add(new PieEntry((float) category.getTotalIncomes(), category.getName()));
                incomesColors.add(Color.parseColor(category.getColor()));
            }
        }

        List<PieEntry> entries = expensesByCategory;

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(entries, "");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueTextColor(Color.WHITE);

        pieDataSet.setColors(expensesColors);

        //add legend to chart
        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setTextSize(12);
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);

        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.notifyDataSetChanged();
        pieChart.invalidate();
    }

    private void syncFromCloud (MovementService movementService) {
        final String token = preferences.getString("token", "test");
        final Call<List<MovementDTO>> call = movementService.findAll(uid, token);

        call.enqueue(new Callback<List<MovementDTO>>() {
            @Override
            public void onResponse(Call<List<MovementDTO>> call, Response<List<MovementDTO>> response) {
                if (response.isSuccessful()) {
                    List<MovementDTO> movements = response.body();
                    for(MovementDTO movement : movements) {
                        Log.d("Movement from API", movement.toString());
                        MovementEntity movementEntity = movementDAO.findById(movement.getId());

                        if(movementEntity != null) {
                            String fromApiUpdate = "" + movement.getUpdatedAt();
                            String fromBBDDUpdate = "" + movementEntity.getUpdatedAt();

                            // FIXME: Hay que arreglar las fechas
                            Log.d(
                                    "IS UPDATABLE?",
                                    (movementEntity.getUpdatedAt() < movement.getUpdatedAt()) + " " + fromBBDDUpdate + " after " + fromApiUpdate
                            );
                            if (movementEntity.getUpdatedAt() < movement.getUpdatedAt()) {
                                movementDAO.update(MovementMapper.toEntity(movement));
                                Toast.makeText(getBaseContext(), "Se sincronizaron movimientos", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            movementDAO.insert(MovementMapper.toEntity(movement));
                            Toast.makeText(getBaseContext(), "Se sincronizaron nuevos movimientos", Toast.LENGTH_LONG).show();
                        }
                    }

                    // RefreshData!

                    ArrayList<MovementEntity> refreshedMovements = movementDAO.findAll(3);

                    if (refreshedMovements.isEmpty()) {
                        llNoData.setVisibility(View.VISIBLE);
                        llBalance.setVisibility(View.GONE);
                        llMovements.setVisibility(View.GONE);
                        llChart.setVisibility(View.GONE);
                    } else {
                        llNoData.setVisibility(View.GONE);
                        llBalance.setVisibility(View.VISIBLE);
                        llMovements.setVisibility(View.VISIBLE);
                        llChart.setVisibility(View.VISIBLE);
                    }

                    balance = calculationDAO.getBalance();
                    tvBalance.setText(NumberFormat.getInstance().format(balance) + " CLP");

                    movementAdapter = new MovementAdapter(Dashboard.this, MovementMapper.toViewModel(refreshedMovements));
                    lvMovements.setAdapter(movementAdapter);
                    movementAdapter.notifyDataSetChanged();

                    categories = categoryDAO.findAllWithMovements(uid);
                    addDataSet(categories);

                    for(MovementEntity movement : refreshedMovements) {
                        Log.d("Movement from DATABASE", movement.toString());
                    }

                } else {
                    Toast.makeText(getBaseContext(), "Error en la respuesta. CODE " + response.code(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<MovementDTO>> call, Throwable t) {
                Toast.makeText(getBaseContext(), "Error al comunicarse con el servicio", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }


    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }
}
