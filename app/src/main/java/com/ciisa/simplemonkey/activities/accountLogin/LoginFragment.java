package com.ciisa.simplemonkey.activities.accountLogin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.ciisa.simplemonkey.activities.dashboard.Dashboard;
import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.core.utils.AccessToken;
import com.ciisa.simplemonkey.core.utils.FormError;
import com.ciisa.simplemonkey.core.utils.InputValidator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;
import javax.inject.Provider;

import dagger.android.support.DaggerFragment;

public class LoginFragment extends DaggerFragment {
    @Inject SharedPreferences preferences;
    @Inject Provider<InputValidator> inputValidatorProvider;
    @Inject FirebaseAuth firebaseAuth;

    TextInputLayout tilEmail, tilPassword;
    Button btnLogin;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ui_fragment_layout, container, false);
        tilEmail = view.findViewById(R.id.tilEmail);
        tilPassword = view.findViewById(R.id.tilPassword);
        btnLogin = view.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(v -> loginUser(tilEmail, tilPassword));
        return view;
    }

    private void loginUser(final TextInputLayout tilEmail, final TextInputLayout tilPassword) {
        String email = tilEmail.getEditText().getText().toString();
        String password = tilPassword.getEditText().getText().toString();

        InputValidator inputValidator = inputValidatorProvider.get();
        inputValidator.isEmail(tilEmail);
        inputValidator.isRequired(tilPassword);

        if(inputValidator.validate()) {
            firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    user.getIdToken(true).addOnCompleteListener(task2 -> {
                        if (task2.isSuccessful()) {
                            SharedPreferences.Editor preferencesEditor = preferences.edit();
                            String tokenResult = task2.getResult().getToken();
                            preferencesEditor.putString("uid", user.getUid());
                            preferencesEditor.putString("token", tokenResult);
                            preferencesEditor.apply();
                        } else {
                            AccessToken.refresh(getContext(), preferences);
                        }
                        Intent intent = new Intent(getActivity(), Dashboard.class);
                        startActivity(intent);
                        getActivity().finish();
                    });
                } else {
                    FormError.vibrate(getContext());
                    Toast.makeText(getActivity(), task.getException() != null ? task.getException().getLocalizedMessage() : "No message", Toast.LENGTH_SHORT).show();
                    tilPassword.getEditText().setText("");
                    tilPassword.setError(getString(R.string.login_error));
                }
            });
        }
    }
}
