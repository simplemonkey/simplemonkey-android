package com.ciisa.simplemonkey.activities.movement;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MovementModule {
    /**
     * Esta metodo nos permite añadir al map de dependencias de dagger android el class AccountLogin
     * @return
     */
    @ContributesAndroidInjector
    abstract MovementCreate contributeMovementCreate();

    @ContributesAndroidInjector
    abstract MovementDetail contributeMovementDetail();

    @ContributesAndroidInjector
    abstract MovementEdit contributeMovementEdit();
}