package com.ciisa.simplemonkey.activities.accountLogin;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.activities.accountRegister.AccountRegister;

import javax.inject.Inject;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class AccountLogin extends AppCompatActivity implements HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> androidInjector;

    @Inject
    LoginFragment loginFragment;

    TextView tvRegister,tvForgotPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Aca se hace la injeccion de las dependencias de mi Activity
        // estas dependencias estan anotadas con @Inject
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_login);

        // REFERENCIAS
        getSupportFragmentManager().beginTransaction().replace(R.id.form_container, loginFragment).commit();

        tvRegister = findViewById(R.id.tvRegister);
        tvForgotPass = findViewById(R.id.tvForgotPass);

        // TEXTVIEW REDIRECT A REGISTRO USUARIO
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AccountRegister.class);
                startActivity(intent);
            }
        });

        // TEXTVIEW REDIRECT A OLVIDAR OCNTRASEÑA
        tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ResetPassword.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }

}
