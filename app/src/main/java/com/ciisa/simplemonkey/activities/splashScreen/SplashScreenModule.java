package com.ciisa.simplemonkey.activities.splashScreen;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SplashScreenModule {
    /**
     * Esta metodo nos permite añadir al map de dependencias de dagger android el class AccountLogin
     * @return
     */
    @ContributesAndroidInjector
    abstract SplashScreen contributeSplashScreen();
}
