package com.ciisa.simplemonkey.activities.movement;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.core.adapter.CategoryAdapter;
import com.ciisa.simplemonkey.core.controller.RetrofitClient;
import com.ciisa.simplemonkey.core.dao.CategoryDAO;
import com.ciisa.simplemonkey.core.dao.MovementDAO;
import com.ciisa.simplemonkey.core.dao.MovementSyncDAO;
import com.ciisa.simplemonkey.core.model.category.Category;
import com.ciisa.simplemonkey.core.model.category.CategoryBuilder;
import com.ciisa.simplemonkey.core.model.category.CategoryDTO;
import com.ciisa.simplemonkey.core.model.category.CategoryMapper;
import com.ciisa.simplemonkey.core.model.movement.MovementBuilder;
import com.ciisa.simplemonkey.core.model.movement.MovementDTO;
import com.ciisa.simplemonkey.core.model.movement.MovementEntity;
import com.ciisa.simplemonkey.core.model.movement.MovementMapper;
import com.ciisa.simplemonkey.core.service.MovementService;
import com.ciisa.simplemonkey.core.ui.DatePickerFragment;
import com.ciisa.simplemonkey.core.utils.DateConvert;
import com.ciisa.simplemonkey.core.utils.FormError;
import com.ciisa.simplemonkey.core.utils.InputValidator;
import com.ciisa.simplemonkey.activities.dashboard.Dashboard;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MovementCreate extends AppCompatActivity implements HasAndroidInjector {

    @Inject DispatchingAndroidInjector<Object> androidInjector;
    @Inject SharedPreferences preferences;
    @Inject MovementService movementService;
    @Inject CategoryDAO categoryDAO;
    @Inject MovementDAO movementDAO;
    @Inject MovementSyncDAO movementSyncDAO;

    private TextInputLayout tilName, tilAmount, tilDate, tilDescription;
    private Spinner spType, spCategory;
    private Button btnRegister;
    private CategoryAdapter categoryAdapter;
    private ArrayAdapter<String> movementTypes;
    private String[] typesArray;

    private void showDatePickerDialog(final TextInputLayout til) {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            final String selectedDate = String.format("%d-%02d-%02d", year, (month +1), day);
            til.getEditText().setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_movement_create);

        // REFERENCIA
        btnRegister = findViewById(R.id.btnRegister);
        tilName = findViewById(R.id.tilName);
        tilAmount = findViewById(R.id.tilAmount);
        tilDate = findViewById(R.id.tilDate);
        tilDescription = findViewById(R.id.tilDescription);
        spType = findViewById(R.id.spType);
        spCategory = findViewById(R.id.spCategory);

        // Escucha e instancia el DatePicker
        tilDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(tilDate);
            }
        });

        categoryAdapter = new CategoryAdapter(
                this,
                CategoryMapper.toViewModel(categoryDAO.findAll(100))
        );
        spCategory.setAdapter(categoryAdapter);

        typesArray =  getResources().getStringArray(R.array.movement_types);
        movementTypes = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, typesArray);
        spType.setAdapter(movementTypes);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String token = preferences.getString("token", "default");
                String uid = preferences.getString("uid", "default");
                String name = tilName.getEditText().getText().toString();
                String amount = tilAmount.getEditText().getText().toString();
                String date = tilDate.getEditText().getText().toString();
                String description = tilDescription.getEditText().getText().toString();
                boolean income = isIncome((String) spType.getSelectedItem());
                int categoryId = (int) spCategory.getSelectedItemId();

                InputValidator inputValidator = new InputValidator(v.getContext());

                inputValidator.isRequired(tilName);
                inputValidator.isNumber(tilAmount);
                inputValidator.isRequired(tilAmount);
                inputValidator.isRequired(tilDate);
                inputValidator.isRequired(tilDescription);

                if(inputValidator.validate()) {
                    Category category = new CategoryBuilder()
                            .withId(categoryId)
                            .build();

                    MovementEntity movement = new MovementBuilder(
                            uid,
                            name,
                            description,
                            date,
                            Double.parseDouble(amount),
                            "CLP",
                            income,
                            true
                            )
                            .withCategory(category)
                            .buildEntity();
                    MovementDTO inserted = MovementMapper.toDTO(movementDAO.insert(movement));
                    if(inserted != null) {
                        Call<Void> createCall = movementService.insert(uid, token, inserted);

                        createCall.enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(v.getContext(), "Movimiento registrado y sincronizado con éxito", Toast.LENGTH_SHORT).show();
                                    movementSyncDAO.delete(inserted.getId());
                                } else {
                                    Log.d("RESPONSE ERROR", Integer.toString(response.code()));
                                    Toast.makeText(v.getContext(), "Movimiento registrado con éxito, pero no sincronizado por un error desconocido", Toast.LENGTH_SHORT).show();
                                }
                                Intent intent = new Intent(MovementCreate.this, Dashboard.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {
                                Toast.makeText(v.getContext(), "Movimiento registrado con éxito, pero no sincronizado por un error de red", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(MovementCreate.this, Dashboard.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        });

                    } else {
                        Toast.makeText(v.getContext(), "Error al escribir en sqlite", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    FormError.vibrate(v.getContext());
                    Toast.makeText(v.getContext(), "Campos Erroneos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    boolean isIncome (String type) {
        return type.equals(typesArray[1]);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }
}
