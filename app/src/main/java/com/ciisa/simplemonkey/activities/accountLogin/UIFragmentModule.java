package com.ciisa.simplemonkey.activities.accountLogin;

import com.ciisa.simplemonkey.activities.accountLogin.LoginFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class UIFragmentModule {
    @ContributesAndroidInjector
    abstract LoginFragment contributeUIFormFragment();
}
