package com.ciisa.simplemonkey.activities.accountLogin;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module(
    includes =  {
        UIFragmentModule.class
    }
)
public abstract class AccountLoginModule {
    /**
     * Esta metodo nos permite añadir al map de dependencias de dagger android el class AccountLogin
     * @return
     */
    @ContributesAndroidInjector(modules = {Internal.class})
    abstract AccountLogin contributeAccountLogin();

    @Module
    public static class Internal {
        @Provides
        LoginFragment provideFragment(){
            return new LoginFragment();
        }

    }
}
