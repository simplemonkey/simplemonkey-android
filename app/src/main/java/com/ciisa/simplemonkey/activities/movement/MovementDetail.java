package com.ciisa.simplemonkey.activities.movement;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ciisa.simplemonkey.R;
import com.ciisa.simplemonkey.core.dao.CategoryDAO;
import com.ciisa.simplemonkey.core.dao.MovementDAO;
import com.ciisa.simplemonkey.core.dao.MovementSyncDAO;
import com.ciisa.simplemonkey.core.model.movement.MovementMapper;
import com.ciisa.simplemonkey.core.model.movement.MovementViewModel;
import com.ciisa.simplemonkey.core.service.MovementService;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovementDetail extends AppCompatActivity implements HasAndroidInjector {

    @Inject DispatchingAndroidInjector<Object> androidInjector;
    @Inject SharedPreferences preferences;
    @Inject MovementService movementService;
    @Inject MovementDAO movementDAO;
    @Inject MovementSyncDAO movementSyncDAO;

    private TextView tvName,tvAmount,tvDescription,tvCategory,tvDate,tvType;
    private Button btnDelete,btnEdit;
    private MovementViewModel movement;
    private String id, token, uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        id = getIntent().getStringExtra("id");

        setContentView(R.layout.activity_movement_detail);
        AndroidInjection.inject(this);

        tvName = findViewById(R.id.tvName);
        tvAmount = findViewById(R.id.tvAmount);
        tvDescription = findViewById(R.id.tvDescription);
        tvCategory = findViewById(R.id.tvCategory);
        tvDate = findViewById(R.id.tvDate);
        btnDelete = findViewById(R.id.btnDelete);
        btnEdit = findViewById(R.id.btnEdit);

        uid = preferences.getString("uid", "");
        token = preferences.getString("token", "");

        movementDAO = new MovementDAO(MovementDetail.this, uid);
        movement = MovementMapper.toViewModel(movementDAO.findById(id));

        tvName.setText(movement.getName());
        tvAmount.setText(String.format("%.0f", movement.getAmount()));
        tvDescription.setText(movement.getDescription());
        tvCategory.setText(movement.getCategory().getName());
        tvDate.setText(movement.getDate());

        btnDelete.setOnClickListener(v -> {
            if (movementDAO.deleteById(id)) {
                Call<Void> deleteCall = movementService.delete(uid, id, token);

                deleteCall.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {
                            movementSyncDAO.delete(id);
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {

                    }
                });

                Intent intent = new Intent(v.getContext(), MovementList.class);
                startActivity(intent);
                finish();
                Toast.makeText(v.getContext(), ("Se eliminó el registro"), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(v.getContext(), ("Error al eliminar el registro"), Toast.LENGTH_SHORT).show();
            }
        });

        btnEdit.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), MovementEdit.class);
            intent.putExtra("id",id);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }
}