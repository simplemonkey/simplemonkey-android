package com.ciisa.simplemonkey.appContext;

import android.content.SharedPreferences;
import android.util.Log;

import com.ciisa.simplemonkey.core.di.DaggerSimpleMonkeyComponent;
import com.ciisa.simplemonkey.core.utils.AccessToken;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class SimpleMonkeyApp extends DaggerApplication {
    @Inject SharedPreferences preferences;

    public void refreshToken() {
        AccessToken.refresh(SimpleMonkeyApp.this, preferences);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AccessToken.refresh(SimpleMonkeyApp.this, preferences);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerSimpleMonkeyComponent.factory().create(this);
    }
}
